@setlocal enableextensions
@cd /d "%~dp0"
@echo off

net session >nul 2>&1
if %errorLevel% == 0 (
   echo Administrative permissions confirmed.
) else (
   echo Please run script as admin
   pause
   exit
)

if NOT %USERNAME% == Manufacturer goto NotManufacturer

IF EXIST "flag.txt" (
	goto Program
) ELSE (
    color 03
    echo Please run EnableWindowsFeatures first before running this script
)
goto exit_prog


:NotManufacturer
echo Please sign in as Manufacturer and try again

:Program
color 03
@echo on
powershell.exe "Invoke-Expression scripts\Install.ps1"
del flag.txt
echo Attempting enabling bitlocker
manage-bde -protectors -add C: -rp

:exit_prog
pause
exit
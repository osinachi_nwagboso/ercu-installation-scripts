
How to run installer:



Before you run the installer, run "EnableWindowsFeatures.bat" as an administrator and then restart the system.

When the system restarts successfully run "Install.exe" as an administrator and follow it's instructions carefully.


Note: 


Please ensure that the system is connected to the internet before attempting to run "EnableWindowsFeatures.bat" or the "Install.exe"

Make sure the installation exectables in Scripts\Bin\Programs are the latest versions

Also, the ERCU application may ask the ERCU user for network permissions on first run. The manufacturer will have to give this permission out before deploying the system 
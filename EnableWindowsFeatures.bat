﻿
@echo off
@setlocal enableextensions
@cd /d "%~dp0"

Title Setting up windows services for ERCU



net session >nul 2>&1
if %errorLevel% == 0 (
   echo Administrative permissions confirmed.
) else (
   echo Please run script as admin
   pause
   exit
)
color 03

echo Registering device... 
Slmgr /ipk M7XTQ-FN8P6-TTKYV-9D4CC-J462D
Slmgr /skms kms.xspace.in
Slmgr /ato

echo Installing keyboard filter feature...
DISM /online /NoRestart /Quiet /Enable-Feature /featurename:Client-DeviceLockdown /featurename:Client-KeyboardFilter

echo Installing Shell Launcher feature...
Dism /online /NoRestart /Quiet /Enable-Feature /all /FeatureName:Client-EmbeddedShellLauncher

echo Installing Windows-Defender ApplicationGuard...
Dism /online /NoRestart /Quiet /Enable-Feature /FeatureName:Windows-Defender-ApplicationGuard

echo Removing Windows boot logo...
bcdedit.exe -set {globalsettings} advancedoptions false
bcdedit.exe -set {globalsettings} optionsedit false
bcdedit.exe -set {globalsettings} bootuxdisabled on

echo Enabling powershell scripts...
powershell.exe "set-executionpolicy unrestricted"


echo This sytem will now restart
@echo>"flag.txt"

pause
shutdown.exe /r /t 00
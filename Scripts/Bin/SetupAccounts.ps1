# Enable Remote management
Set-WSManQuickConfig -Force -SkipNetworkProfileCheck



Configuration RedSpeed2 {

    Import-DscResource -ModuleName 'PSDesiredStateConfiguration'
    Import-DscResource -ModuleName 'NetworkingDsc'
    Import-DscResource -ModuleName 'xRemoteDesktopAdmin'
    Import-DscResource -ModuleName 'xPSDesiredStateConfiguration'


    Node "localhost" {
	
        $manufacturepasswd = ConvertTo-SecureString "rs04RF06" -AsPlainText -Force
        $manufacturecreds = New-Object System.Management.Automation.PSCredential ("Manufacturer", $manufacturepasswd)
	
		User Manufacturer
		{
			Ensure = "Present"  
			UserName = "Manufacturer"
			Password = $manufacturecreds 
			PasswordChangeRequired = $false
            PasswordNeverExpires = $true
		} 
        
        $ERCUpasswd = ConvertTo-SecureString "ercu" -AsPlainText -Force
        $ERCUcreds = New-Object System.Management.Automation.PSCredential ("ERCU", $ERCUpasswd)
	
		User ERCUMode
		{
			Ensure = "Present"
			UserName = "ERCU"
			Password = $ERCUcreds 
			PasswordChangeRequired = $false
            PasswordChangeNotAllowed = $true
            PasswordNeverExpires = $true
		}
        
        xGroup Users
        {
            GroupName = "Users"
            Ensure = "Present"
            MembersToInclude  = @("Manufacturer", "ERCU")
            DependsOn = "[User]Manufacturer","[User]ERCUMode"
        }
        
    
		Group Administrators
		{
			GroupName = "Administrators"
			Ensure = "Present"
			MembersToInclude  = "Manufacturer"
            DependsOn = "[User]Manufacturer"
        }

        Registry DisableSerialMouse
        {
            Ensure = "Present"
            Key = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\sermouse"
            ValueName = "Start"
            ValueType = 'Dword'
            ValueData = "4"

        }

        Registry SetAutoLoginUser
        {
            Ensure = "Present"
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
            ValueName = "DefaultUserName"
            ValueType = 'String'
            ValueData = "ERCU"

        }

        Registry SetAutoLoginPassword
        {
            Ensure = "Present"
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
            ValueName = "DefaultPassword"
            ValueType = 'String'
            ValueData = "ercu"

        }

        Registry EnableAutoLogin
        {
            Ensure = "Present"
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
            ValueName = "AutoAdminLogon"
            ValueType = 'String'
            ValueData = "1"

        }


        File CopyLockScreenImage
        {
            Ensure = "Present"
            SourcePath = "$PSScriptRoot\lockScreenImage.png"
            DestinationPath = "C:\ProgramData\lockScreenImage.png"
        }

        Registry SetLockScreenImage
        {
            Ensure = "Present"
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Personalization"
            ValueName = "LockScreenImage"
            ValueData = "C:\ProgramData\lockScreenImage.png"
            DependsOn = "[File]CopyLockScreenImage"
        }

        WindowsOptionalFeature DisableInternetExplorer
        {
            Ensure = 'Disable'
            Name = "Internet-Explorer-Optional-amd64"
        }

        WindowsOptionalFeature DisableWindowsMediaPlayer
        {
            Ensure = 'Disable'
            Name = "WindowsMediaPlayer"
        }

        WindowsOptionalFeature EnableShellLauncher
        {
            Ensure = 'Enable'
            Name = 'Client-EmbeddedShellLauncher'
        }


        Firewall RSRemoteConfiguration
        {
            Name = "RS Remote Configuration"
            LocalPort = '443'
            Protocol = 'TCP'
            Ensure = 'Present'
            Enabled = 'True'
            Action = 'Block'
            Profile = ('Domain','Public','Private')
        }

        Firewall RemoteDesktopConfiguration
        {
            Name = "Remote Desktop - User Mode (TCP-In)"
            Ensure = 'Present'
            Enabled = 'True'
            LocalPort = '3389'
            Action = 'Block'
            Profile = ('Domain','Public','Private')

        }

        FirewallProfile EnablePrivateFirewall
        {
            Name = 'Private'
            DefaultInboundAction = 'Block'
            Enabled = 'True'

        }

        FirewallProfile EnablePublicFirewall
        {
            Name = 'Public'
            DefaultInboundAction = 'Block'
            Enabled = 'True'

        }

        FirewallProfile EnableDomainFirewall
        {
            Name = 'Domain'
            DefaultInboundAction = 'Block'
            Enabled = 'True'

        }

        xRemoteDesktopAdmin EnableRemoteDesktop
        {
            Ensure = 'Absent'
            UserAuthentication = 'Secure'
        }


	
    }
}

$ConfigData = @{ AllNodes = @(@{ NodeName= 'localhost'; PsDscAllowPlainTextPassword = $true})}
RedSpeed2 -ConfigurationData $ConfigData

# Apply configuration
Start-DscConfiguration -Wait -Force -Verbose  -Path .\RedSpeed2

# Enable bitlocker
manage-bde -on C:

# Disable Sleep
powercfg.exe -x -standby-timeout-ac 0
powercfg.exe -x -hibernate-timeout-ac 0


# Disable Windows Remote Management - Needed for DSC
Stop-Service WinRM
Set-Service -Name WinRM -StartupType Disabled
$code = @"
using System;
using System.IO;
using System.Diagnostics;

namespace Installer
{
	public class Program
	{
		public static void Main()
		{
			string filePath = "scripts\\bin\\Programs";

			string[] files = Directory.GetFiles(filePath);

			foreach (var program in files)
			{
				Process p = Process.Start(program);
				p.WaitForExit();
			}
		}
	}
}
"@
 
Add-Type -TypeDefinition $code -Language CSharp	
iex "[Installer.Program]::Main()"
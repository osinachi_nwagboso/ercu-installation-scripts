



# Policy binary files should be named as {GUID}.cip for multiple policy format files (where {GUID} = <PolicyId> from the Policy XML)
$PolicyBinary = "scripts\bin\wdac\{927887E2-1F2E-4731-B42A-23F074A7E240}.cip"
$DestinationFolder = $env:windir+"\System32\CodeIntegrity\CIPolicies\Active\"
$RefreshPolicyTool = "scripts\bin\wdac\RefreshPolicy(AMD64).exe"

Copy-Item -Path $PolicyBinary -Destination $DestinationFolder -Force
& $RefreshPolicyTool


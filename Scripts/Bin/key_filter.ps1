﻿param (
    [String] $ComputerName
)

$CommonParams = @{"namespace"="root\standardcimv2\embedded"}
$CommonParams += $PSBoundParameters

function Enable-Predefined-Key($Id) {
    <#
    .Synopsis
        Toggle on a Predefined Key keyboard filter Rule
    .Description
        Use Get-WMIObject to enumerate all WEKF_PredefinedKey instances,
        filter against key value "Id", and set that instance's "Enabled"
        property to 1/true.
    .Example
        Enable-Predefined-Key "Ctrl+Alt+Del"
        Enable CAD filtering
#>

    $predefined = Get-WMIObject -class WEKF_PredefinedKey @CommonParams |
        where {
            $_.Id -eq "$Id"
        };

    if ($predefined) {
        $predefined.Enabled = 1;
        $predefined.Put() | Out-Null;
        Write-Host Enabled $Id
    } else {
        Write-Error "$Id is not a valid predefined key"
    }
}


# Some example uses of the functions defined above.
Enable-Predefined-Key "Ctrl+Alt+Del"
Enable-Predefined-Key "Shift+Ctrl+Esc"
Enable-Predefined-Key "Win+L"


$COMPUTER = "localhost"
$NAMESPACE = "root\standardcimv2\embedded"

# Define the decimal scan code of the Home key

$HomeKeyScanCode = 0

# Get the BreakoutKeyScanCode setting from WEKF_Settings

$BreakoutMode = get-wmiobject -class wekf_settings -namespace $NAMESPACE | where {$_.name -eq "BreakoutKeyScanCode"}

# Set the breakout key to the Home key.

$BreakoutMode.value = $HomeKeyScanCode

# Push the change into the WMI configuration. You must restart your device before this change takes effect.

$BreakoutMode.put()
Invoke-Expression scripts\bin\InstallerLauncher.ps1

# Install DSC additional modules
Install-Module -Name NetworkingDsc
Install-Module -Name XRemoteDesktopAdmin
Install-Module -Name xPSDesiredStateConfiguration

Invoke-Expression scripts\bin\SetupAccounts.ps1
Invoke-Expression scripts\bin\set_wallpaper.ps1
Invoke-Expression scripts\bin\custom_shell.ps1

Set-WinUserLanguageList -LanguageList en-GB, en-GB -Force
Invoke-Expression scripts\bin\key_filter.ps1

Invoke-Expression scripts\bin\wdac\wdac_setup.ps1

Pause